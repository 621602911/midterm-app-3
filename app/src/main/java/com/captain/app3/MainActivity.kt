package com.captain.app3

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.captain.app3.model.Oil

class MainActivity : AppCompatActivity() {
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val oils : List<Oil> = listOf(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20:" , 36.84),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91:" , 37.68),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95:" , 37.95),
            Oil("เชลล์ วีพาวเวอร์ แก๊สโซฮอล์ 95:" , 45.44),
            Oil("เชลล์ ดีเซล B20:" , 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล:" , 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7:" , 36.34),
            Oil("เชลล์ วีพาวเวอร์ ดีเซล:" , 36.34),
            Oil("เชลล์ วีพาวเวอร์ ดีเซล B7:" , 47.06),
        )
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(oils)
    }
    class ItemAdapter(val oils : List<Oil>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
        class ViewHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView){
            val oil: TextView = itemView.findViewById<TextView>(R.id.oil)
            val price: TextView = itemView.findViewById<TextView>(R.id.price)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item , parent ,false)
            return ViewHolder(adapterLayout)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.oil.text = oils[position].oil
            holder.price.text = oils[position].price.toString()
        }

        override fun getItemCount(): Int {
            return oils.size
        }
    }
}