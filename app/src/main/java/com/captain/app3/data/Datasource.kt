package com.captain.app3.data

import android.annotation.SuppressLint
import com.captain.app3.model.Oil


class DataSource {
    @SuppressLint("ResourceType")
    fun loadData(): List<Oil>{
        return listOf<Oil>(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20:" , 36.84),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91:" , 37.68),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95:" , 37.95),
            Oil("เชลล์ วีพาวเวอร์ แก๊สโซฮอล์ 95:" , 45.44),
            Oil("เชลล์ ดีเซล B20:" , 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล:" , 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7:" , 36.34),
            Oil("เชลล์ วีพาวเวอร์ ดีเซล:" , 36.34),
            Oil("เชลล์ วีพาวเวอร์ ดีเซล B7:" , 47.06),

            )
    }
}