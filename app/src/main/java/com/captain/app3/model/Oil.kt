package com.captain.app3.model

import android.annotation.SuppressLint
import androidx.annotation.StringRes
@SuppressLint("SupportAnnotationUsage")
data class Oil (
    @StringRes val oil: String,
    @StringRes val price: Double
)